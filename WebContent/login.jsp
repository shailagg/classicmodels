<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" session="true" %>

<c:if test="${not empty sessionScope.customer}">
	<c:redirect url="catalogo.jsp" />
</c:if>

<!DOCTYPE html>
<html>
	<head>
	<meta charset="UTF-8">
	<title>Inicio de Sesión</title>
	</head>
	<body>
		<form action="auth" method="post">
			<p><label for="email">eMail</label></p>
			<p><input type="email" name="email" required="required" /></p>
			<p><label for="password">Contraseña</label></p>
			<p><input type="password" name="password" required="required"/></p>
			<p><input type="submit" value="Login" /></p>
		</form>	
		<c:choose>
			<c:when test="${param.status == 1}">
				<p>Las credenciales del usuario no son válidas</p>
			</c:when>
			<c:when test="${param.status == 2}">
				<p>Error del sistema, contacte con el administrador</p>
			</c:when>
		</c:choose>
	</body>
</html>
